defmodule Airbnb do
  def aggr_count_properties() do
    File.stream!("listings.csv")
    |> CSV.decode!(headers: true)
    |> Enum.take(6)
    |> Enum.map(fn p -> p["neighbourhood_cleansed"] end)
    |> Enum.frequencies
  end
end
